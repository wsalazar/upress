uPoke
=========
This is a Micro Framework I am creating as a learning to become a better Object-Oriented Developer/Engineer.

Deployment Etiquette
--------------------
Classes must be unit tested using PHPUnit.

1. All code must be unit tested and pass 100%.
2. Code coverage must be around 90% or higher. We can use the [Coverage] package for this.
3. The code must be run through a Code Sniffer with PEP8 standards.
4. The code must be reviewed by at least 2 other people.

Version Control Etiquette
-------------------
### Branching
When a new feature is created a new branch MUST be created.

```unix
git checkout -b add_some_feature
git add .
git commit -m 'added some feature in the newly feature branch'
git push origin add_some_feature
git checkout master
git merge --no-ff add_some_feature
```

### Tagging
Once you have successfully pushed a feature branch into master, you will want to tag the releases. This is so that we can always go back to a stable copy of the codebase. You will want to tag in that branch, not master.
```unix
git tag tag__add_some_feature
git push --tags
```

Comments
---------
Each PHP file should be properly commented otherwise it should not be pushed to git.